# Programmi in Python per il corso di "Machine Learning con Applicazioni".
Le note ed i programmi in questa repository sono parte integrante del suddetto corso al Dipartimento di Fisica dell'Univestità degli Studi di Milano.
Parte del materiale presente nei Python notebooks di questa repository deriva da tutorial standard e dagli esempi usati dal Prof. M. Hjorth-Jensen l'omonimo corso all'università di Oslo (il quale è sentitamente ringraziato). Tutti i file qui presenti sono stati creati e/o adattati da C. Barbieri per il **primo semestre, a.a. 2024/25**.

La componente computazionale del corso usa *python 3.x* e fogli note (notebooks) di *jupyter*. I notebook in ogni cartella approfondiscono particolari argomenti trattati a lezione e forniscono una serie di esempi già risolti dell'applicazione di Python al machine learning.

Il materiale verrà discusso il più estesamente possibile durante le lezioni al Laboratorio di Calcolo, mentre gli esempi rimanenti possono essere tenuti per ulteriori approfondimenti oltre che come referenza per l'implementazione del progetto finale. 

Per accedere alla repository: [https://gitlab.com/cbarbieri/MachineLearning-con-applicazioni](https://gitlab.com/cbarbieri/MachineLearning-con-applicazioni). (**NB:** I juputer notebook possono anche essere scaricati direttamente dal sito usando l'icona a destra vicino al pulsante 'clone'.)


## Come aggiornare la copia locale della repository
Man mano che il nuovo materiale viene aggiunto durante il semestre, si può aggiornare la copia locale della repository come segue (se già clonata come descritto più avanti):

```bash
$ cd MachineLearning-con-applicazioni
$ git pull
```
È bene ricordarsi di **cambiare il nome dei file di notebook** su cui si sta lavorando, per non sovrascriverne il contenuto.


## Primi Passi
Aprire un terminale e muoversi nella cartella in cui si intende lavorare (`cd` in unix/linux):
```bash
$ cd My_folder
```
Per utilizzare i notebook è necessario avere istallati *python* e *jupiter lab*, oltre che altre librerie standard discusse mano a mano negli esempi (vedere anche i notebook di introduzione a Python). Dai computer del *Laboratorio di Calcolo* in dipartimento è sufficiente attivare l'istallazione di anaconda
```bash
$ module load python3/anaconda
```
Sul proprio computer, conviene controllare che Anaconda e python siano presenti (con tutta probablità, altre distribuzioni come *miniconda* e *Canopy* funzionano altrettanto bene). Per esempio: 
```bash
$ which python
/home/username/anaconda3/bin/python
```
Per clonare la repository usare uno dei seguenti comandi:
```bash
$ git clone https://gitlab.com/cbarbieri/MachineLearning-con-applicazioni.git
oppure
$ git clone git@gitlab.com:cbarbieri/MachineLearning-con-applicazioni.git
```
Questo crea una cartella `MachineLearning-con-applicazioni` (N.B.: la seconda opzione, via ssh, può richiedere un account su GitLab ed una public key. Se dà errori, usare `https` invece). Una volta entrati nella caterlla, jupiter viene invocato digitando `jupyter lab` da terminale:
```bash
$ cd MachineLearning-con-applicazioni
$ jupyter lab
```
Questo apre una nuova tavola nel browser all'indirizzo `localhost:8888`. Cliccando sulla cartella `Elementi di Python` e poi su `PythonIntro_Numpy_e_Panda.ipynb`, si è pronti ad iniziare!


## Istallare jupyter sul proprio computer
Risovere gli esercizi richiede una versione recente di `python`, che includa `jupyter`, `numpy`, `matplotlib` e alcune altre librerie standard in python. Se queste non sono presenti, la soluzione migliore per evitare di interferire con eventuali python già presenti nel sistema operativo è quella di istallare [Anaconda](https://www.anaconda.com/download/), la quale mantiene una distribuszione completa e aggiornata di python e relativi strumenti per vari sistemi operativi.  Dal sito qui sopra scegliere **python 3.x** ed il proprio OS per scaricare ed istallare il tutto. 

  Se la programmazione ad oggetti **con Phyton** (funzioni, classi, liest, array, ciclim plot) vi e nuova, è possibile trovare dei tutorial per principlanti ai seguenti link:

[https://sites.engineering.ucsb.edu/~shell/che210d/python.pdf](https://sites.engineering.ucsb.edu/~shell/che210d/python.pdf) della UC Santa barbara. Questo in stile manuale.

Capitoli 2,3,4,5 di 
[https://cforssen.gitlab.io/tif285-book/content/Intro/welcome.html](https://cforssen.gitlab.io/tif285-book/content/Intro/welcome.html) di C. Forssen. Questo docimento contiene anche materiale sull'approacio di Bayes sull'inferenza statistica.


## Ricerca di esempi in rete
In certe situazioni può diventare utile ed efficiente **riadattare codici altrui** o prenderne esempio (purché la paternità del codice venga propriamente riconosciuta!). Nel risolvere questi esercizi, *non* è richiesto rifare tutto da capo, cercare informazioni in rete riguardo a  "*how to do X in python*" è ammesso. Alcuni siti che riportano informazioni utili sono:

+ Un buon forum dove cercare informazioni di natura computazionale:
  + [Stackoverflow](https://stackoverflow.com/)

+ Documentazioni ufficiali:
  + [Numpy documentation](https://docs.scipy.org/doc/numpy/reference/routines.html)
  + [Scipy documentation](https://docs.scipy.org/doc/scipy/reference/)
  + [Jupyter Lab documentation](https://jupyterlab.readthedocs.io/en/stable/)

+ Tutorial per Pyhon:
  + [Data Flair Python Tutorials](https://data-flair.training/blogs/python-tutorials-home/)
  
## Ricevimento
Gli studenti in necessità di aiuto o chiarimenti sono vivamente invitati a contattare il docente  `carlo.barbieri@unimi.it` oppure a presentarsi durante l'ora di ricevimento (martedì 14:00-15:00, info su MyAriel). Per orari diversi mandate una mail in anticipo.


C. Barbieri,  6 ottobre 2024.
